# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)
a = [int(s) for s in input().split()]
index = 0
for i in range(1, len(a)):
    if a[i] > a[index]:
        index = i
print(a[index], index)